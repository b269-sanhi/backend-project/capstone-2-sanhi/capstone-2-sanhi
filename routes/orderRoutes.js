const express = require("express")
const router = express.Router()
const orderControllers = require("../controllers/orderControllers")
const userControllers = require("../controllers/userControllers")
const auth = require("../auth")

// Router for user create order
router.post("/create", (req,res)  => {
	let data = {
		reqBody: req.body,
		authToken: auth.decode(req.headers.authorization)
	}
	orderControllers.create(data).then(resultFromController => res.send(resultFromController));
})

//route for retrieve authenticated user order
router.get("/:orderId", (req,res) => {
	orderControllers.allOrders(req.params).then(resultFromController => res.send(resultFromController));
})

//route for retrieve all orders (ADMIN only)
router.get("/all/orders", auth.verify, (req,res) => {
	let data = {
		isAdmin: auth.decode(req.headers.authorization).isAdmin
	}
	orderControllers.adminAllOrders(data).then(resultFromController => res.send(resultFromController));
})
module.exports = router;