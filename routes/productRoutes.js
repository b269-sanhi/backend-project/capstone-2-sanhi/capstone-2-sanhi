const express = require("express")
const router = express.Router()
const productControllers = require("../controllers/productControllers")
const auth = require("../auth")


router.post("/create", auth.verify, (req, res) => {

	const data = {
		product: req.body,
		isAdmin: auth.decode(req.headers.authorization).isAdmin
	}
	productControllers.addProduct(data).then(resultFromController => res.send(resultFromController));
});

// route for getting all active products
router.get("/active", (req, res) => {
    productControllers.getActiveProducts().then(resultFromController => res.send(resultFromController));
});

// Route for retrieving all the products
router.get("/all", (req,res) => {
	productControllers.getAllProducts().then(resultFromController => res.send(resultFromController));
})


//  Route retrieving specific course
router.get("/:productId", (req,res) => {
	productControllers.getProducts(req.params).then(resultFromController => res.send(resultFromController));
})


// Routes for updating a products
router.put("/:productId", (req,res) => {
	productControllers.updateProducts(req.params, req.body).then(resultFromController => res.send(resultFromController)
		);
})

// change status
router.patch("/:courseId/archive", auth.verify, (req,res) => {
	productControllers.updateArchive(req.params, req.body).then(resultFromController => res.send(resultFromController)
		);
})


//route for retrieve all orders (ADMIN only)
router.get("/all/products", auth.verify, (req,res) => {
	let data = {
		isAdmin: auth.decode(req.headers.authorization).isAdmin
	}
	productControllers.adminAllProducts(data).then(resultFromController => res.send(resultFromController));
})


module.exports = router