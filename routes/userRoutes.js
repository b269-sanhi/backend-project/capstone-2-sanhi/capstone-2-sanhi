const express = require("express")
const router = express.Router()
const userController = require("../controllers/userControllers")
const orderController = require("../controllers/orderControllers")
const productController = require("../controllers/productControllers")
const auth = require("../auth")

// Route for checking if the user's email already exists in the database
router.post("/checkEmail", (req, res) => {
	userController.checkEmailExists(req.body).then(resultFromController => res.send(resultFromController))
})

// Route for user registration
router.post("/register", (req,res) => {
	userController.registerUser(req.body).then(resultFromController => res.send(resultFromController))
})

// route for user authentication
router.post("/login", (req,res) => {
	userController.loginUser(req.body).then(resultFromController => res.send(resultFromController))
})


// Route for retrieving user details
router.post("/details", auth.verify,(req, res) => {

	const userData = auth.decode(req.headers.authorization)

userController.getProfile({userId : userData.id}).then(resultFromController => res.send(resultFromController));
});


module.exports = router