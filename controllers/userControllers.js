const User = require("../models/User");
const Order = require("../models/Order");
const Product = require("../models/Product");

const bcrypt = require("bcrypt")
const auth = require("../auth")
// Check if email already exists

module.exports.checkEmailExists = (reqBody) => {
	return User.find({email: reqBody.email}).then(result =>{
		if(result.length > 0){
			return true
		}else{
			return false
		}
	})
}
// User registration
module.exports.registerUser = (reqBody) => {
	let newUser = new User ({
		firstName: reqBody.firstName,
		lastName: reqBody.lastName,
		email: reqBody.email,
		mobileNo: reqBody.mobileNo,
		password: bcrypt.hashSync(reqBody.password, 10)
	})
	return newUser.save().then((user,error) =>   {
		if (error){
		return false
		} else {
			return true
		}
	})
}


// User authentication
module.exports.loginUser = (reqBody) => {
	return User.findOne({email: reqBody.email}).then(result =>{
		if (result == null){
			return false
		} else if (reqBody.password == null){
			return "Please put password"
		} else {
			const isPasswordCorrect = bcrypt.compareSync(reqBody.password, result.password);	
			if (isPasswordCorrect) {
				return {access: auth.createAccessToken(result)}
			} else {
				return "Correct password is needed";
			};
		};
	});
};


// Retrieve User details
module.exports.getProfile = (data) => {
	return User.findById(data.userId).then(result => {
		result.password = "";
		return result;
	});
};