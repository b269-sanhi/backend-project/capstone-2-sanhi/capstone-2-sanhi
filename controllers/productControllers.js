const Product = require("../models/Product");
const bcrypt = require("bcrypt")
const auth = require("../auth")

module.exports.addProduct = (data) => {
	// User is an admin
	if (data.isAdmin) {
		let newProduct = new Product({
			name : data.product.name,
			description : data.product.description,
			price : data.product.price,
			stock : data.product.stock
			
		});
		// Saves the created object to our database
		return newProduct.save().then((product, error) => {
			// Course creation successful
			if (error) {
				return false;
			// Course creation failed
			} else {
				return true;
			};
		});
	} 
	// User is not an admin
	let message = Promise.resolve ('User must be ADMIN to access this!')
	return message.then((value) => {{
		return {value}
	}})
	// return false
};

// Retrieve ALL products
module.exports.getAllProducts = () =>{
	return Product.find({}).then(result =>{
		return result
	})
}

// Retrive specific courses
module.exports.getProducts = (reqParams) =>{
	return Product.findById(reqParams.productId).then(result =>{
		return result
	})
}

// Update a products
module.exports.updateProducts = (reqParams, reqBody) => {
	let updateProducts = {
		name : reqBody.name,
		description : reqBody.description,
		price : reqBody.price,
		stock : reqBody.stock
	}
	return Product.findByIdAndUpdate(reqParams.productId, updateProducts).then((course,error) => {
		if(error){
			return false
		}else {
			return true
		}
	})
}

// change status
module.exports.updateArchive = (reqParams, reqBody) => { 	
	let updatedArchive = {
		
		isActive: reqBody.isActive
		// isActive: false
	}
	return Product.findByIdAndUpdate(reqParams.productId, updatedArchive).then((course, err) => { 		
		if (err){ 
			console.log(err)		
		return false; 	
	} else
		 { 			return true
		} 	
	})
	 }


// retrieve all products (ADMIN only)
module.exports.adminAllProducts = (data) => {
	if (data.isAdmin){
		return Product.find({}).then(result => {return result})
	}	;
	let message = Promise.resolve ('User must be ADMIN to access this!')
	return message.then((value) => {
		return value
	});
};

// retrieving all ACTIVE products
module.exports.getActiveProducts = () => {
    return Product.find({isActive: true}).then(result => {
        return result;
    });
};