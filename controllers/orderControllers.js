const Order = require("../models/Order");
const Product = require("../models/Product");
const bcrypt = require("bcrypt")
const auth = require("../auth")

// User order

module.exports.create = async (data) => {
	try {
		const product = await Product.findById(data.reqBody.productId);

		if (!product) {
			throw new Error('Product not found');
		}

		const newOrder = new Order({
			userId: data.authToken.id,
			products: [{
				productId: data.reqBody.productId,
				quantity: data.reqBody.quantity
			}],
			totalAmount: product.price * data.reqBody.quantity,
			purchasedOn: new Date()
		});

		console.log(newOrder);

		const savedOrder = await newOrder.save();

		console.log(savedOrder);

		return true;
	} catch (err) {
		console.error(err);
		return false;
	}
};




// retrieve authenticated user order
module.exports.allOrders = (reqParams) => {
	return Order.findById(reqParams.orderId).then(result => {
		return result;
	});
};
// retrieve all orders (ADMIN only)
module.exports.adminAllOrders = (data) => {
	if (data.isAdmin){
		return Order.find({}).then(result => {return result})
	}	;
	let message = Promise.resolve ('User must be ADMIN to access this!')
	return message.then((value) => {
		return value
	});
};