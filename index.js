// Setup the dependencies
const express = require("express")
const mongoose = require("mongoose")
const cors = require("cors")
const orderRoute = require("./routes/orderRoutes")
const productRoute = require("./routes/productRoutes")
const userRoute = require("./routes/userRoutes")
// Server setup
const app = express()

// Middlewares
app.use(cors())
app.use(express.json())
app.use(express.urlencoded({extended:true}))
app.use("/users", userRoute)
app.use("/products", productRoute)
app.use("/orders", orderRoute)
// Database connection
mongoose.connect("mongodb+srv://psanhi24:admin123@zuitt-bootcamp.asvy4zs.mongodb.net/capstoneProjectEcommerce?retryWrites=true&w=majority",
	{	
		useNewUrlParser: true,
		useUnifiedTopology: true
	})
mongoose.connection.once("open",() => console.log(`Now connected to the cloud database!`))

// Server listening
app.listen(process.env.PORT || 5000, () => console.log(`Now connected to port ${process.env.PORT || 5000}`))