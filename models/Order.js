const mongoose = require("mongoose")

const orderSchema = new mongoose.Schema({

	userId: {
		type: String,
		required: [true, "Please put your userId"]
	},

	products: [
	{
		productId: {
			type : String,
		required : [true, "Please put your productId"]
		},
		quantity: {
			type : Number,
			required: [true, "Quantity is required"]
		}
	}
		],
	totalAmount: {
		type: Number,
		required: [true, "The total amount is:"]
	},
	purchasedOn: {
		type: Date,
		default : new Date()
	}
});

module.exports = mongoose.model("Order", orderSchema)